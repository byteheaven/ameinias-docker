FROM openjdk:11

MAINTAINER tekHedd <tekhedd@byteheaven.net>

# Force UTF-8 locale so file names on the mp3 shares will
# make sense.

ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8

# Get ameinias from the official download at tekhedd.com:
# TEST: v2572
# v3.1.0.1 - 2574
# v3.2.0 - 2587
# v3.3.0 - 2771

RUN cd /usr/local \
  && curl -SLs https://tekhedd.com/?ddownload=2771 -o /tmp/ameinias.zip \
  && unzip /tmp/ameinias.zip \
  && rm /tmp/ameinias.zip \
  && mkdir /adata \
  && mkdir /mp3 \
  && chmod 0755 ameinias/docker/bootstrap.sh

# If you don't mount an MP3 dir, you'll be disappointed
# adata should be persistent as it is where we store the database

VOLUME [ "/adata", "/mp3" ]

EXPOSE 7076

WORKDIR /usr/local/ameinias
CMD [ "/usr/local/ameinias/docker/bootstrap.sh", "ameinias" ]
