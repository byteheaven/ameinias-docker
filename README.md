# ameinias-docker

Creates an Ameinias jukebox container that writes audio output to
a fifo. Requires ameinias-ices2-docker and ameinias-icecast2-docker to
actually serve ogg-vorbis streams, or anything that can read raw pcm.
