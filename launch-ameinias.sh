#!/bin/bash

# To use a remote h2 database, change the connection string
# in the environment:
#
# -e CONNECTION_STRING=jdbc:h2:tcp://mp3host/ameinias;TRACE_LEVEL_FILE=4;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE


exec docker run --name ameinias \
    --rm -it \
    -p 80:7076 \
    -v /var/ameinias:/adata \
    -v /var/mp3:/mp3 \
    tekhedd/ameinias:3.0.5 \
    "$@"
